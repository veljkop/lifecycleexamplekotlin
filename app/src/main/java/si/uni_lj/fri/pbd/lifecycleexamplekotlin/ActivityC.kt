package si.uni_lj.fri.pbd.lifecycleexamplekotlin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import si.uni_lj.fri.pbd.lifecycleexamplekotlin.databinding.ActivityCBinding

class ActivityC: AppCompatActivity() {

    private lateinit var binding: ActivityCBinding

    companion object {
        const val TAG = "ActivityC"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")

        super.onCreate(savedInstanceState)

        binding = ActivityCBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.button.setOnClickListener {
            startActivity(Intent(applicationContext, ActivityA::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT))

        }
    }


    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "onRestart")
    }
}